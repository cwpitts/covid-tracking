$(document).ready(function onReady(){
		$(".plot").on("click", function imgClicked(){
				let imgSrc = $(this).attr("src");
				let img = `<img src=${imgSrc} class="img-fluid w-100">`;
				$("#image-modal-body").empty();
				$("#image-modal-body").append(img);
				$("#image-modal").modal("show");
		});
});
