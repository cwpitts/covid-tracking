#!/usr/bin/env python3
# https://thematicmapping.org/downloads/world_borders.php
# https://opendata.ecdc.europa.eu/covid19/
import os
from pathlib import Path

import geopandas as gpd
import matplotlib.animation as mpla
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import requests

plt.style.use("fivethirtyeight")

data_dir = Path("data")
output_dir = Path("output")
shapefile = Path(
    data_dir / "shapefiles" / "world" / "UIA_World_Countries_Boundaries.shp"
)
cmap = "plasma"  # "Wistia"

for d in [data_dir, output_dir]:
    if not d.exists():
        os.mkdir(d.resolve())
if not (output_dir / "world").exists():
    os.mkdir(output_dir / "world")
    os.mkdir(output_dir / "world" / "frames")

resp = requests.get("https://opendata.ecdc.europa.eu/covid19/casedistribution/csv")
with open(data_dir / "world_daily.csv", "w") as out_file:
    out_file.write(resp.text)

df = pd.read_csv(data_dir / "world_daily.csv")

# Clean data
df.dateRep = pd.to_datetime(df.dateRep, format="%d/%m/%Y")
df = df.rename(columns={"dateRep": "date", "geoId": "country"})
df = df.dropna()
map_df = gpd.read_file(shapefile).rename(columns={"ISO": "country"})

# Chloropleth
fig, ax = plt.subplots(figsize=(10, 6))
deaths_agg = df.groupby("country")["deaths"].agg("sum")
merged = map_df.set_index("country").join(deaths_agg).dropna()
merged.plot(column="deaths", cmap=cmap, linewidth=1.0, edgecolor="0.6", ax=ax)
sm = plt.cm.ScalarMappable(
    cmap=cmap,
    norm=plt.Normalize(
        vmin=merged.deaths.min(), vmax=merged.deaths.max()
    ),
)
sm._A = []
cbar = fig.colorbar(sm)
ax.axis("off")
ax.tick_params(axis="x", labelsize=8)
ax.annotate(
    "Source: European Centers for Disease Control, 2020",
    xy=(0.1, 0.08),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color="#555555",
)
ax.set_title("COVID-19 Deaths")
fig.savefig(
    output_dir / "world" / "world_chloropleth.png", dpi=600, bbox_inches="tight"
)
plt.close("all")

# Per-capita death chloropleth
fig, ax = plt.subplots(figsize=(10, 6))
capita_deaths_agg = df.groupby("country")[["deaths", "popData2019"]].agg("sum")
capita_deaths_agg.deaths = (
    capita_deaths_agg.deaths / capita_deaths_agg.popData2019
)
log_deaths_agg = np.log(capita_deaths_agg)
log_deaths_agg[np.isinf(log_deaths_agg)] = 0
merged = map_df.set_index("country").join(log_deaths_agg).dropna()
merged.plot(column="deaths", cmap=cmap, linewidth=1.0, edgecolor="0.6", ax=ax)
sm = plt.cm.ScalarMappable(
    cmap=cmap,
    norm=plt.Normalize(
        vmin=capita_deaths_agg.deaths.min(),
        vmax=capita_deaths_agg.deaths.max(),
    ),
)
sm._A = []
cbar = fig.colorbar(sm)
# cbar.ax.yaxis.set_ticklabels(list(map(lambda x: f"{x:.0f}", np.e ** cbar.get_ticks())))
ax.axis("off")
ax.tick_params(axis="x", labelsize=8)
ax.annotate(
    "Source: European Centers for Disease Control, 2020",
    xy=(0.1, 0.08),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color="#555555",
)
ax.set_title("COVID-19 Deaths Per Capita")
fig.savefig(
    output_dir / "world" / "world_chloropleth_capita.png", dpi=600, bbox_inches="tight"
)
plt.close("all")

# Line plots
fig, ax = plt.subplots(figsize=(10, 6))
g = df.groupby("date")
cases_agg = g["cases"].agg("sum")
deaths_agg = g["deaths"].agg("sum")
ax.plot(cases_agg.index, cases_agg, label="Cases")
ax.plot(deaths_agg.index, deaths_agg, label="Deaths", color="red")
ax.legend()
ax.set_title("COVID-19 Worldwide Cases and Deaths")
ax.tick_params(axis="x", labelsize=8)
ax.annotate(
    "Source: European Centers for Disease Control, 2020",
    xy=(0.1, 0.04),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color="#555555",
)
fig.savefig(output_dir / "world" / "spread_world.png", dpi=300, bbox_inches="tight")
plt.close("all")

# Stacked bar chart
# Top countries by cases
country_group = df.groupby("country")
country_cases_agg = country_group["cases"].agg("sum")
country_deaths_agg = country_group["deaths"].agg("sum")
most_cases_idx = country_cases_agg.nlargest(10).index
fig, ax = plt.subplots(figsize=(10, 6))
ax.bar(most_cases_idx, country_deaths_agg[most_cases_idx], label="Deaths")
ax.bar(
    most_cases_idx,
    country_cases_agg[most_cases_idx],
    bottom=country_deaths_agg[most_cases_idx],
    label="Cases",
)
ax.legend()
ax.annotate(
    "Source: European Centers for Disease Control, 2020",
    xy=(0.1, 0.04),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color="#555555",
)
ax.set_title("Cases and Deaths for Ten Most Affected Countries")
fig.savefig(
    output_dir / "world" / "world_cases_deaths_bar.png", dpi=300, bbox_inches="tight"
)
plt.close("all")
