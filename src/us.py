#!/usr/bin/env python3
# https://www.arcgis.com/home/item.html?id=1a6cae723af14f9cae228b133aebc620
# https://www.arcgis.com/home/item.html?id=540003aa59b047d7a1f465f7b1df1950
# https://www.winwaed.com/blog/2019/09/17/converting-esri-lpk-files-to-shapefiles-for-use-in-qgis/
import os
import sys
from pathlib import Path

import geopandas as gpd
import matplotlib.colors as mpc
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import requests

plt.style.use("fivethirtyeight")

data_dir = Path("data")
output_dir = Path("output")
shapefile = data_dir / "shapefiles" / "usa" / "states.gdb/"
annot_color = "#555555"
annot_pos = (0.1, 0.02)
xlabel_size = 6
xlabel_rot = -90
figsize = (10, 8)
cmap = "plasma"  # "Wistia"

for d in [data_dir, output_dir]:
    if not d.exists():
        os.mkdir(d.resolve())
if not (output_dir / "us").exists():
    os.mkdir(output_dir / "us")

resp = requests.get("https://covidtracking.com/api/v1/states/daily.csv")
if not resp.ok:
    print(f"Bad HTTP request: {resp.status_code}")
    sys.exit(0)
with open(data_dir / "us_daily.csv", "w") as out_file:
    out_file.write(resp.text)

df = pd.read_csv(data_dir / "us_daily.csv")
df.date = pd.to_datetime(df.date.astype(str))
map_df = gpd.read_file(shapefile)
map_df = map_df.rename(columns={"STATE_ABBR": "state"})

# Chloropleth
fig, ax = plt.subplots(figsize=(10, 6))
merged = map_df.set_index("state").join(
    df[df.date == df.date.iloc[0]][["state", "death"]].set_index("state")
)
merged.plot(column="death", cmap=cmap, linewidth=1.0, edgecolor="0.6", ax=ax)
sm = plt.cm.ScalarMappable(
    cmap=cmap, norm=plt.Normalize(vmin=merged.death.min(), vmax=merged.death.max())
)
sm._A = []
cbar = fig.colorbar(sm)
ax.axis("off")
ax.annotate(
    "Source: Covid Tracking Project, 2020",
    xy=(0.1, 0.1),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color=annot_color,
)
ax.set_title("COVID-19 Deaths in the United States")
fig.savefig(output_dir / "us" / "states_chloropleth.png", dpi=600, bbox_inches="tight")
plt.close("all")

# Per-capita chloropleth
fig, ax = plt.subplots(figsize=(10, 6))
capita_df = df[df.date == df.date.iloc[0]][["state", "death"]].set_index("state")
state_pop = map_df[["state", "POP2010"]].set_index("state")
capita_df.death = capita_df.death / state_pop.POP2010
capita_df = capita_df.dropna()
merged = map_df.set_index("state").join(np.log(capita_df))
merged.plot(column="death", cmap=cmap, linewidth=1.0, edgecolor="0.6", ax=ax)
sm = plt.cm.ScalarMappable(
    cmap=cmap, norm=plt.Normalize(vmin=merged.death.min(), vmax=merged.death.max()),
)
sm._A = []
cbar = fig.colorbar(sm)
cbar.ax.yaxis.set_ticklabels(list(map(lambda x: f"{x:.5f}", np.e ** cbar.get_ticks())))
ax.axis("off")
ax.annotate(
    "Source: Covid Tracking Project, 2020",
    xy=(0.1, 0.1),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color=annot_color,
)
ax.set_title(
    "COVID-19 Deaths Per Capita in the United States",
    fontdict={"fontsize": 16},
)
fig.savefig(
    output_dir / "us" / "states_chloropleth_capita.png", dpi=600, bbox_inches="tight"
)
plt.close("all")

# Line plots
# All states
n_states = 5
highs = {}
for state in df.state.unique():
    highs[state] = df[df.state == state].positiveIncrease.max()
ranked = sorted(highs.items(), key=lambda x: x[1])
highest = ranked[-n_states:]
lowest = ranked[:n_states]

fig, ax = plt.subplots(figsize=(10, 6))
for state, positives in highest:
    idx = df.state == state
    ax.plot(df[idx].date, df[idx].positiveIncrease, label=state)
    ax.text(df[idx].iloc[0].date, df[idx].iloc[0].positiveIncrease, state)
ax.set_title("COVID-19 New Positive Tests")
ax.annotate(
    "Source: Covid Tracking Project, 2020",
    xy=(0.25, 0.7),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color=annot_color,
)
ax.tick_params(axis="x", labelsize=12)
fig.savefig(output_dir / "us" / "positive_topfive.png", bbox_inches="tight")
plt.close("all")

# Positive/negative for all states
g = df.groupby("date")
pos_agg = g["positive"].agg("sum")
neg_agg = g["negative"].agg("sum")
death_agg = g["death"].agg("sum")
pos_change_agg = g["positiveIncrease"]
fig, ax = plt.subplots(figsize=(10, 6))
ax.plot(pos_agg.index, pos_agg, label="Positive")
ax.plot(death_agg.index, death_agg, label="Deaths")
ax.legend()
ax.set_title("COVID-19 Cases and Deaths - United States")
ax.annotate(
    "Source: Covid Tracking Project, 2020",
    xy=(0.1, 0.03),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color=annot_color,
)
ax.tick_params(axis="x", labelsize=12)
fig.savefig(output_dir / "us" / "spread_us.png", bbox_inches="tight")
plt.close("all")

fig, ax = plt.subplots(figsize=(10, 6))
ax.plot(neg_agg.index, neg_agg, label="Negative")
ax.plot(pos_agg.index, pos_agg, label="Positive")
ax.legend()
ax.set_title("COVID-19 Spread - United States")
ax.annotate(
    "Source: Covid Tracking Project, 2020",
    xy=(0.1, 0.03),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color=annot_color,
)
ax.tick_params(axis="x", labelsize=12)
fig.savefig(output_dir / "us" / "posneg_us.png", bbox_inches="tight")
plt.close("all")

# Stacked bars
# Top countries by cases
states_group = df.groupby("state")
states_cases_agg = states_group["positive"].agg("sum")
states_deaths_agg = states_group["death"].agg("sum")
fig, ax = plt.subplots(figsize=(20, 6))
ax.bar(states_deaths_agg.index, states_deaths_agg, label="Deaths")
ax.bar(
    states_cases_agg.index, states_cases_agg, bottom=states_deaths_agg, label="Cases"
)
ax.set_yscale("log")
ax.legend()
ax.tick_params(axis="x", labelsize=8)
ax.annotate(
    "Source: COVID Tracking Project, 2020",
    xy=(0.1, 0.04),
    xycoords="figure fraction",
    horizontalalignment="left",
    verticalalignment="top",
    fontsize=12,
    color="#555555",
)
ax.set_title("Cases and Deaths for All States")
fig.savefig(output_dir / "us" / "cases_deaths_states.png", dpi=600, bbox_inches="tight")
plt.close("all")
